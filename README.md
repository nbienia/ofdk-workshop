# (O)ber(f)inanz(d)irektion - (K)arlsruhe

Gitlab workshop to getting started with gitlab ci.

## Feature/jobs

> A atomar build step composed in a acyclic graph

1. app job
1. test job
1. both jobs with default before and after scripts
1. app job overwrites before and after scripts
1. test job will only triggered on develop and master branch
1. pipeline fail when job fail

> `./.feature-jobs/.gitlab-ci.yaml.*` contains the different pipelines

## Feature/stages

> A group of jobs with same the logical build time point (default: test)

1. build stage
1. test stage
1. run test despite build error (pipeline fail)
1. success pipeline despite build error
1. manual deploy step
1. added int-test job to stage:test
1. deploy depends on int-test job
1. added variable TEST to manual int-test
1. differentiate int-test for different branches

> `./.feature-stages/.gitlab-ci.yml.*` contains the different pipelines

## Features/syntax

> Yaml with some reserved keywords

1. yaml
1. yaml multilines
1. yaml anchors for scripts
1. yaml anchors for merges
1. yaml anchors for job extension
1. use extend instead

> `./.feature-syntax/.gitlab-ci.yml.*` contains the different pipelines

## Feature/artifacts

> Produced files from a job to get later access

1. Save artifact
1. Use artifact from previous job with needs
1. Use artifact from previous job with dependencies
1. Save test report as artifact
1. Add Projekt Cache
1. Add Pipeline Cache (Single Cache)

> `./.feature-artifacts/.gitlab-ci.yml.*` contains the different pipelines

## Feature/environment

> Environment variable handling

1. Show predefined env vs export
1. Pipeline variables
2. Ignore default pipeline variables
1. Global variables
1. Manual overwrites
1. Global env variables
1. Enviroments variables staging
1. Enviroments variables staging+prod

> `./.feature-environments/.gitlab-ci.yml.*` contains the different pipelines

## Feature/include - templating

> Share job declaration in multiple files (+across multiple projects)

1. Local include
1. Overwrite/merge include job and variables
1. Add new stage:deploy (failed)
1. Add new stage:deploy (success)
1. Add a project:file include
1. Set a special branch of project:file include
1. Add a remote include

> `./.feature-include/.gitlab-ci.yml.*` contains the different pipelines

## Feature/trigger - multi project

> Multi project pipeline to trigger downstrem projects

1. Trigger downstream project
1. Trigger downstream project with variables
1. Trigger downstream project with artifacts
1. Trigger downstream project with strategy:depend

> `./.feature-trigger/.gitlab-ci.yml.*` contains the different pipelines

## Feature/parent - child ~ trigger+include

> Multi project pipeliness + includes to combine pros of both features

1. Trigger+include local pipeline
1. Trigger+include local and remote pipeline
1. Trigger+include fail sample
1. Trigger+include strategy:depend
1. Trigger+include with child execution rule
1. Trigger+include with parent execution rule

> `./.feature-parent/.gitlab-ci.yml.*` contains the different pipelines
